## Task
Implement `Article` component which accept next props:
- `title` - should be displayed inside `h1` tag
- `date` - should be displayed inside a `span` tag
- `text` - should be displayed inside `p` tag

`App` component should render `Article` component and pass props from `article` object.